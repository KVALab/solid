﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SOLID
{
    public class ValidatorParam : IValidate
    {
        public bool Validate<T>(IEnumerable<T> enumerable)
        {           
            var param = enumerable.Cast<int>().ToList();
            return (param.Count() >= 3 && 
                    param.All(p => p > 0) &&
                    param.First() < param.Skip(1).First() &&
                    param.Last() < param.Skip(1).First());
        }
    }
}