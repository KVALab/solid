﻿using System;

namespace SOLID
{
    public class Game
    {
        private readonly IConsoleReader _consoleReader;
        private readonly IParser<int> _parserParam;
        private readonly IValidate _validator;
        private readonly IGenerator _generatorNumber;

        public Game(IConsoleReader consoleReader, IParser<int> parserParam, IValidate validator, IGenerator generatorNumber)
        {
            _consoleReader = consoleReader;
            _parserParam = parserParam;
            _validator = validator;
            _generatorNumber = generatorNumber;
        }

        public void StartGame()
        {
            WriteRules();

            int[] params_game = _parserParam.Parse(_consoleReader.Read());

            if (! _validator.Validate(params_game))
            {
                _consoleReader.Write("Введены не корректные данные для старта игры!");
                return;
            }

            int rnd_value = _generatorNumber.Generate(params_game[0], params_game[1]);
            int count_pass = params_game[2];

            int current_pass = 0;
            while (current_pass < count_pass)
            {
                _consoleReader.Write("Ваше число:");
                if (int.TryParse(_consoleReader.Read(), out int current_value))
                {
                    if (current_value > rnd_value)
                        _consoleReader.Write("Ваше число больше загаданного!");
                    else if (current_value < rnd_value)
                        _consoleReader.Write("Ваше число меньше загаданного!");
                    else if (current_value == rnd_value)
                    {
                        _consoleReader.Write($"Вы угадали число: {rnd_value} с {current_pass + 1} попытки!");
                        break;
                    }

                    current_pass++;
                }
            }

            if (current_pass == count_pass)
                _consoleReader.Write($"Вы не угадали загаданное число: {rnd_value}.");
        }

        private void WriteRules()
        {
            _consoleReader.Write($"Введите начальное и конечное значение для генерации числа в заданном диапазоне,{Environment.NewLine}" +
                $"количество попыток для возможности отгадать заданное число.{Environment.NewLine}" +
                $"Введите 3 цифровых значения разделенных пробелом, пример: 1 10 3");
        }
    }
}
