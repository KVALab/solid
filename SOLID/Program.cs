﻿
namespace SOLID
{
    class Program
    {
        static void Main(string[] args)
        {
            new Game(
                new ConsoleReader(), 
                new ParserParam<int>(), 
                new ValidatorParam(), 
                new GeneratorNumber())
                .StartGame();
        }
    }
}
