﻿using System;

namespace SOLID
{
    public class GeneratorNumber : IGenerator
    {
        public int Generate(int v1, int v2)
        {
            return new Random().Next(v1, v2);
        }
    }
}
