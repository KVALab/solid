﻿namespace SOLID
{
    public interface IParser<T>
    {
        T[] Parse(string in_str);
    }
}