﻿namespace SOLID
{
    public interface IReader
    {
        string Read();
    }
}