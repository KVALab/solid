﻿namespace SOLID
{
    public interface IConsoleReader : IReader, IWriter
    {   
    }
}