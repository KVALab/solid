﻿namespace SOLID
{
    public interface IWriter
    {
        void Write(string str);
    }
}