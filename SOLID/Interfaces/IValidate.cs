﻿using System.Collections.Generic;

namespace SOLID
{
    public interface IValidate
    {
        public bool Validate<T>(IEnumerable<T> enumerable);
    }
}