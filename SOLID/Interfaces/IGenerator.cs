﻿namespace SOLID
{
    public interface IGenerator
    {
        int Generate(int v1, int v2);
    }
}