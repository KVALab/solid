﻿using System;
using System.Linq;

namespace SOLID
{
    public class ParserParam<T> : IParser<T>
    {
        public T[] Parse(string in_str)
        {
            if (in_str == null || string.IsNullOrEmpty(in_str))
                throw new NullReferenceException();

            return in_str
                .Split(new char[] { ' ' })
                .Select(v => Convert.ChangeType(v, typeof(T)))
                .Cast<T>()
                .ToArray();
        }
    }
}
