﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID
{
    public class ConsoleReader : IConsoleReader
    {
        public string Read() => Console.ReadLine();
        public void Write(string in_str) => Console.WriteLine(in_str);
    }
}
